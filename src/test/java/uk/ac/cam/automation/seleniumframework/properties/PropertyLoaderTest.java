package uk.ac.cam.automation.seleniumframework.properties;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PropertyLoaderTest {

    @Test
    public void testLoadProperty() {
        PropertyLoader.registerPropertiesFileConfig(new PropertiesFileConfig("properties/test.properties"));
        assertEquals("test1", PropertyLoader.getProperty("test.property"));
    }

    @Test
    public void testLoadPropertySystem() {
        PropertyLoader.registerPropertiesFileConfig(new PropertiesFileConfig("properties/test.properties"));
        assertEquals("test1", PropertyLoader.getProperty("test.property"));
        System.setProperty("test.property", "I override you");
        assertEquals("I override you", PropertyLoader.getProperty("test.property"));
        System.clearProperty("test.property");
    }

}
