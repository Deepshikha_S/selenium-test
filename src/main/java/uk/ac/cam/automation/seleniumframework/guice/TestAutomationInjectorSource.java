package uk.ac.cam.automation.seleniumframework.guice;

import com.google.inject.*;
import io.cucumber.guice.CucumberModules;
import io.cucumber.guice.CucumberScopes;
import io.cucumber.guice.InjectorSource;
import io.cucumber.guice.ScenarioScope;
import uk.ac.cam.automation.seleniumframework.driver.DriverManager;
import uk.ac.cam.automation.seleniumframework.properties.guice.PropertiesModule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TestAutomationInjectorSource implements InjectorSource {

    @Override
    public Injector getInjector() {
        ScenarioScope delegate = CucumberScopes.createScenarioScope();
        ScenarioScope decorator = new ScenarioScope() {
            @Override
            public void enterScope() {
                delegate.enterScope();
                DriverManager.getDriver();
            }

            @Override
            public void exitScope() {
                DriverManager.clearDriver();
                delegate.exitScope();
            }

            @Override
            public <T> Provider<T> scope(Key<T> key, Provider<T> provider) {
                return delegate.scope(key, provider);
            }
        };

        Module scenarioModule = CucumberModules.createScenarioModule(decorator);

        return Guice.createInjector(Stage.PRODUCTION, getModulesAndAddScenarioModule((AbstractModule) scenarioModule));
    }

    /*
  Override this method to provide additional Guice modules.
  @return
 */
    protected List<AbstractModule> provideAdditionalModules() {
        return Collections.emptyList();
    }

    private Iterable<? extends AbstractModule> getModulesAndAddScenarioModule(AbstractModule scenarioModule) {
        List<AbstractModule> modules = new ArrayList<>();
        modules.addAll(Arrays.asList(new PropertiesModule(), scenarioModule));
        modules.addAll(provideAdditionalModules());
        return modules;
    }

}