package uk.ac.cam.automation.seleniumframework.email;

import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import java.util.Comparator;
import java.util.Properties;

public class BaseEmailService {

    protected static String mailHost;
    protected static String protocol;
    protected static String port;

    protected static Integer timeoutInSeconds;

    protected static Comparator<Message> messageComparator;

    static {
        mailHost = PropertyLoader.getProperty(CommonProperties.MAIL_HOST);
        protocol = PropertyLoader.getProperty(CommonProperties.MAIL_RETRIEVAL_PROTOCOL);
        port = PropertyLoader.getProperty(CommonProperties.MAIL_RETRIEVAL_PORT);
        timeoutInSeconds = PropertyLoader.getProperty(CommonProperties.MAIL_TIMEOUT_IN_SECONDS) == null ? null : Integer.parseInt(PropertyLoader.getProperty(CommonProperties.MAIL_TIMEOUT_IN_SECONDS));

        messageComparator = ((m1, m2) -> {
            try {
                return m2.getSentDate().compareTo(m1.getSentDate());
            } catch (MessagingException e) {
                return 0;
            }
        });
    }

    protected static Store openMailClient() throws MessagingException {
        validateProperties();

        Properties props = System.getProperties();
        props.setProperty("mail.store.protocol", protocol);
        props.setProperty("mail.imap.port", port);
        final Session session = Session.getInstance(props, null);
        return session.getStore(protocol);

    }


    private static void validateProperties() {
        if (mailHost == null) {
            throw new MailRetrievalException("There is no mail host configured to retrieve email, please ensure the " + CommonProperties.MAIL_HOST + " property is set");
        }
        if (protocol == null) {
            throw new MailRetrievalException("There is no mail protocol configured to retrieve email, please ensure the " + CommonProperties.MAIL_RETRIEVAL_PROTOCOL + " property is set");
        }
        if (port == null) {
            throw new MailRetrievalException("There is no mail retrieval port configured to retrieve email, please ensure the " + CommonProperties.MAIL_RETRIEVAL_PORT + " property is set");
        }
        if (timeoutInSeconds == null) {
            throw new MailRetrievalException("There is no mail timeout configured to retrieve email, please ensure the " + CommonProperties.MAIL_TIMEOUT_IN_SECONDS + " property is set");
        }
    }

}
