package uk.ac.cam.automation.seleniumframework.database;

import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public abstract class BaseDao {

    private JdbcTemplate jdbcTemplate;

    public BaseDao() {
        jdbcTemplate = new JdbcTemplate(constructDatasource());
    }

    public abstract DataSource constructDatasource();

    protected JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
