package uk.ac.cam.automation.seleniumframework.email.domain;

import javax.mail.Message;

public class Recipient {
    private final String emailAddress;
    private String name;
    private Message.RecipientType recipientType;

    public Recipient(String emailAddress, String name) {
        this.recipientType = Message.RecipientType.TO;
        this.emailAddress = emailAddress;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public Message.RecipientType getRecipientType() {
        return this.recipientType;
    }
}
