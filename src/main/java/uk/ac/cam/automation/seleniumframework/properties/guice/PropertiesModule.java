package uk.ac.cam.automation.seleniumframework.properties.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import uk.ac.cam.automation.seleniumframework.properties.PropertiesFileNotFoundException;

import java.io.IOException;
import java.util.Properties;

public class PropertiesModule extends AbstractModule {

        @Override
        protected void configure() {
            try {
                Properties properties = new Properties();
                properties.load(getClass().getClassLoader().getResourceAsStream("properties/test-automation.properties"));
                Names.bindProperties(binder(), properties);
            } catch (IOException e) {
                throw new PropertiesFileNotFoundException(e);
            }
        }
    };