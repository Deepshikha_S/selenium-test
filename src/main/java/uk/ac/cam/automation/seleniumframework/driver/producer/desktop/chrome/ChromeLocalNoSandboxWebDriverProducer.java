package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.chrome;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import uk.ac.cam.automation.seleniumframework.driver.DriverCreationException;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.CommonProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

public class ChromeLocalNoSandboxWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        if (System.getProperty("webdriver.chrome.driver") == null) {
            String chromeDriver = PropertyLoader.getProperty(CommonProperties.SELENIUM_DRIVER_PATH_CHROME);
            if (chromeDriver == null) {
                throw new DriverCreationException("You have specified Chrome Local as your browser but have not set the " + CommonProperties.SELENIUM_DRIVER_PATH_CHROME + " property either as a command line argument or in a registered properties file.");
            }

            System.setProperty("webdriver.chrome.driver", chromeDriver);
        }

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");

        return new ChromeDriver(chromeOptions);
    }

}
