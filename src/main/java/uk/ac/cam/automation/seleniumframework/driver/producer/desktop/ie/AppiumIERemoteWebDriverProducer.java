package uk.ac.cam.automation.seleniumframework.driver.producer.desktop.ie;

import io.appium.java_client.windows.WindowsDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import uk.ac.cam.automation.seleniumframework.driver.GridUtils;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;

public class AppiumIERemoteWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName", "WindowsPC");
        capabilities.setCapability("app", "C:\\Program Files\\Internet Explorer\\iexplore.exe");
        WindowsDriver<WebElement> remoteWebDriver = new WindowsDriver<>(GridUtils.getSeleniumGridURL(), capabilities);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        remoteWebDriver.execute("taskkill /F /IM iexplore.exe /T");
        remoteWebDriver.execute("taskkill /F /IM jp2launcher.exe /T");
        return remoteWebDriver;
    }

}