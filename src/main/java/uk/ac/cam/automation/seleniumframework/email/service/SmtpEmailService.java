package uk.ac.cam.automation.seleniumframework.email.service;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.automation.seleniumframework.email.EmailFailedException;
import uk.ac.cam.automation.seleniumframework.email.domain.Email;
import uk.ac.cam.automation.seleniumframework.email.domain.FailedRecipient;
import uk.ac.cam.automation.seleniumframework.email.domain.Recipient;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileTypeMap;
import javax.activation.MimetypesFileTypeMap;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class SmtpEmailService implements EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmtpEmailService.class);
    private final String mailServerHost;
    private final int mainServerPort;
    private String charset = "utf-8";
    private String subtype = "html";
    private FileTypeMap mimetypesFileTypeMap = new MimetypesFileTypeMap();

    public SmtpEmailService(String mailServerHost, int mailServerPort) {
        this.mailServerHost = mailServerHost;
        this.mainServerPort = mailServerPort;
    }

    public List<FailedRecipient> sendEmailsIndividually(Email email) throws EmailFailedException {
        List<FailedRecipient> failedList = new ArrayList();
        Session session = this.constructSession();
        Transport transport = null;

        try {
            try {
                transport = session.getTransport("smtp");
                transport.connect(this.mailServerHost, this.mainServerPort, (String) null, (String) null);
            } catch (MessagingException var17) {
                throw new EmailFailedException("Unable to establish a connection to the server ", var17);
            }

            Iterator var5 = email.getRecipients().iterator();

            while (var5.hasNext()) {
                Recipient recipient = (Recipient) var5.next();

                try {
                    MimeMessage msg = this.constructMessage(session, email);
                    msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient.getEmailAddress(), recipient.getName()));
                    transport.sendMessage(msg, msg.getAllRecipients());
                } catch (IOException | MessagingException var16) {
                    failedList.add(new FailedRecipient(recipient, var16));
                }
            }
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (Exception var15) {
                LOGGER.error("Failed to close server connection after message sending");
            }

        }

        return failedList;
    }

    public void sendEmailAtomically(Email email) throws EmailFailedException {
        Session session = this.constructSession();
        Transport transport = null;

        try {
            transport = session.getTransport("smtp");
            MimeMessage msg = this.constructMessage(session, email);
            Iterator var5 = email.getRecipients().iterator();

            while (var5.hasNext()) {
                Recipient recipient = (Recipient) var5.next();
                msg.addRecipient(recipient.getRecipientType(), new InternetAddress(recipient.getEmailAddress(), recipient.getName()));
            }

            transport.connect(this.mailServerHost, this.mainServerPort, (String) null, (String) null);
            transport.sendMessage(msg, msg.getAllRecipients());
        } catch (IOException | MessagingException var14) {
            throw new EmailFailedException("Unable to send email atomically ", var14);
        } finally {
            try {
                if (transport != null) {
                    transport.close();
                }
            } catch (Exception var13) {
                LOGGER.error("Failed to close server connection after message sending");
            }

        }
    }

    private Session constructSession() {
        return Session.getDefaultInstance(new Properties());
    }

    private MimeMessage constructMessage(Session session, Email email) throws MessagingException, IOException {
        MimeMessage msg = new MimeMessage(session);
        msg.setSubject(email.getSubject(), this.charset);
        msg.setFrom(new InternetAddress(email.getFrom()));
        if (!email.hasAttachments()) {
            msg.setText(email.getBody(), this.charset, this.subtype);
        } else {
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(email.getBody(), this.charset, this.subtype);
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            Iterator var6 = email.getAttachments().iterator();

            while (var6.hasNext()) {
                String filename = (String) var6.next();
                String contentType = this.mimetypesFileTypeMap.getContentType(filename);
                if (contentType == null || "".equals(contentType)) {
                    LOGGER.warn("Unable to determine content type for: " + filename + ".  Defaulting to: " + "application/octet-stream");
                    contentType = "application/octet-stream";
                }

                DataSource source = new ByteArrayDataSource(IOUtils.toByteArray(email.getAttachments().get(Integer.parseInt(filename))), contentType);
                messageBodyPart = new MimeBodyPart();
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
            }

            msg.setContent(multipart);
        }

        return msg;
    }
}
