package uk.ac.cam.automation.seleniumframework.email;

import javax.mail.Folder;
import javax.mail.MessagingException;

class InboxFolder {

    private Folder inbox;

    public InboxFolder(Folder inbox) {
        this.inbox = inbox;
    }

    public Folder getInbox() {
        return inbox;
    }

    public int getNewMessageCount() {
        try {
            return inbox.getNewMessageCount();
        } catch (MessagingException e) {
            throw new MailRetrievalException("Could not get message count from inbox", e);
        }
    }
}