package uk.ac.cam.automation.seleniumframework.driver.producer.mobile.appium;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import uk.ac.cam.automation.seleniumframework.driver.GridUtils;
import uk.ac.cam.automation.seleniumframework.driver.producer.WebDriverProducer;
import uk.ac.cam.automation.seleniumframework.properties.AppiumMobileProperties;
import uk.ac.cam.automation.seleniumframework.properties.PropertyLoader;

import java.util.Objects;

public class AppiumMobileWebDriverProducer implements WebDriverProducer {

    @Override
    public WebDriver produce() {
        String browserName = PropertyLoader.getProperty(AppiumMobileProperties.BROWSER_NAME);
        String platformName = PropertyLoader.getProperty(AppiumMobileProperties.PLATFORM_NAME);
        String platformVersion = PropertyLoader.getProperty(AppiumMobileProperties.PLATFORM_VERSION);
        String deviceName = PropertyLoader.getProperty(AppiumMobileProperties.DEVICE_NAME);
        String automationName = PropertyLoader.getProperty(AppiumMobileProperties.AUTOMATION_NAME);

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
        caps.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        caps.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
        if (platformName != null) {
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
        }
        if (automationName != null) {
            caps.setCapability(MobileCapabilityType.AUTOMATION_NAME, automationName);
        }
        if (Objects.requireNonNull(platformName).equalsIgnoreCase("android")) {
            caps.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, true);
            caps.setCapability(AndroidMobileCapabilityType.RESET_KEYBOARD, true);
            caps.setCapability(AndroidMobileCapabilityType.NATIVE_WEB_SCREENSHOT, true);
        }
        AppiumDriver<WebElement> remoteWebDriver = new AppiumDriver<>(GridUtils.getSeleniumGridURL(), caps);
        remoteWebDriver.setFileDetector(new LocalFileDetector());
        return remoteWebDriver;
    }

}
